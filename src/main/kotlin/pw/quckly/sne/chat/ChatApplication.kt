package pw.quckly.sne.chat

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Configuration
import org.springframework.http.server.ServerHttpRequest
import org.springframework.http.server.ServerHttpResponse
import org.springframework.http.server.ServletServerHttpRequest
import org.springframework.stereotype.Component
import org.springframework.web.socket.WebSocketHandler
import org.springframework.web.socket.config.annotation.EnableWebSocket
import org.springframework.web.socket.config.annotation.WebSocketConfigurer
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry
import org.springframework.web.socket.server.HandshakeInterceptor
import org.springframework.web.socket.server.support.DefaultHandshakeHandler
import java.lang.Exception
import java.security.Principal
import java.security.cert.X509Certificate

@SpringBootApplication
class ChatApplication

fun main(args: Array<String>) {
    runApplication<ChatApplication>(*args)
}

class ChatPrincipal(val _name: String) : Principal {
    override fun getName() = _name
    override fun toString() = _name
}

class WSSecureChatHandshakeHandler : DefaultHandshakeHandler() {

    override fun determineUser(request: ServerHttpRequest, wsHandler: WebSocketHandler, attributes: MutableMap<String, Any>): Principal? {
        val clientCertificateObj = (request as ServletServerHttpRequest).servletRequest
                .getAttribute("javax.servlet.request.X509Certificate")

        if (clientCertificateObj != null) {
            val clientCertificateArray = clientCertificateObj as Array<X509Certificate>

            if (clientCertificateArray.size == 1) {
                val clientCertificate = clientCertificateArray[0]

                return clientCertificate.subjectDN
            }
        }

        return ChatPrincipal(request.remoteAddress.toString())
    }
}

@Component
class WSSecureChatHandshakeBanChecker : HandshakeInterceptor {
    @Autowired
    lateinit var banSystem: BanSystem

    override fun afterHandshake(request: ServerHttpRequest, response: ServerHttpResponse, wsHandler: WebSocketHandler, exception: Exception?) {
    }

    override fun beforeHandshake(request: ServerHttpRequest, response: ServerHttpResponse, wsHandler: WebSocketHandler, attributes: MutableMap<String, Any>): Boolean {
        return banSystem.checkUser(request.remoteAddress)
    }
}

@Configuration
@EnableWebSocket
class WSConfig : WebSocketConfigurer {
    @Autowired
    var chatHandler: ChatWSHandler? = null

    @Autowired
    lateinit var banChecker: WSSecureChatHandshakeBanChecker

    override fun registerWebSocketHandlers(registry: WebSocketHandlerRegistry) {
        registry.addHandler(chatHandler!!, "/chat")
                .setAllowedOrigins("*")
                .setHandshakeHandler(WSSecureChatHandshakeHandler())
                .addInterceptors(banChecker)
    }
}
