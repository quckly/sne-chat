package pw.quckly.sne.chat

import org.springframework.stereotype.Component
import java.net.InetAddress
import java.net.InetSocketAddress
import java.util.concurrent.CopyOnWriteArrayList

@Component
class BanSystem {
    val bannedUsers = CopyOnWriteArrayList<InetAddress>();

    fun banUser(address: InetSocketAddress) {
        bannedUsers.add(address.address)
    }

    fun unbanUser(address: InetSocketAddress) {
        bannedUsers.remove(address.address)
    }

    fun checkUser(address: InetSocketAddress): Boolean {
        return !bannedUsers.contains(address.address)
    }
}
