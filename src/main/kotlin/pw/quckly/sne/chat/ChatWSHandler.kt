package pw.quckly.sne.chat

import com.fasterxml.jackson.databind.JsonNode
import org.springframework.stereotype.Component
import org.springframework.web.socket.CloseStatus
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.TextWebSocketHandler
import java.util.concurrent.CopyOnWriteArrayList
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.beans.factory.annotation.Autowired

data class Packet(val type: String, val payload: JsonNode)

data class JoinEvent(val roomId: String)
data class UserActionEvent(val userId: String)

data class Attachment(val type: String, val payload: JsonNode)

data class ReceivedMessage(val roomId: String, val text: String, val attachment: Attachment? = null)
data class Message(val user: String, val roomId: String, val text: String, val attachment: Attachment? = null)

data class User(val name: String, val role: String, val session: WebSocketSession)
data class UserView(val name: String, val role: String)
data class UserListPacket(val users: Array<UserView>)
data class UserProfile(val yourName: String, val yourRole: String)
data class Room(val id: String, val name: String) {
    var users = CopyOnWriteArrayList<String>()
    var messages = CopyOnWriteArrayList<Message>()
}

@Component
class ChatWSHandler : TextWebSocketHandler() {

    @Autowired
    lateinit var banSystem: BanSystem

    var jsonMapper = jacksonObjectMapper()

    private final val userList = CopyOnWriteArrayList<User>()
    private final val roomList = CopyOnWriteArrayList<Room>()

    init {
        roomList.add(Room("default", "General"))
    }

    override fun handleTransportError(session: WebSocketSession, exception: Throwable) {
        exception.printStackTrace();
    }

    override fun afterConnectionClosed(session: WebSocketSession, status: CloseStatus) {
        // Remove user
        userList.removeIf {
            it.session == session
        }

        // Notify users
        notifyUpdateUserList()
    }

    override fun handleTextMessage(session: WebSocketSession, textMessage: TextMessage) {
        val packet = jsonMapper.readValue(textMessage.payload, Packet::class.java)
        val user = findUser(session)

        if (user == null) {
            println("User for ${session.remoteAddress} not found.")
            return
        }

        when (packet.type) {
            "message" -> {
                val receivedMessage = jsonMapper.treeToValue(packet.payload, ReceivedMessage::class.java)
                val message = Message(user = user.name,
                        roomId = receivedMessage.roomId,
                        text = receivedMessage.text,
                        attachment = receivedMessage.attachment)

                println("Message from ${user.name} (${session.remoteAddress}) to ${message.roomId}: ${message.text}")

                val room = findRoom(message.roomId)

                if (room == null || !isJoinedIn(user, room)) {
                    println("!!! Access denied. Room doesn't exist. Message from ${user.name} (${session.remoteAddress}) to ${message.roomId}: ${message.text}")
                    return
                }

                sendMessageToRoom(message, room)
            }
            "join" -> {
                val joinEvent = jsonMapper.treeToValue(packet.payload, JoinEvent::class.java)

                val room = findRoom(joinEvent.roomId)

                if (room == null) {
                    println("!!! Access denied. Room doesn't exist. Join event from ${user.name} (${session.remoteAddress})}")
                    return
                }

                joinUserToRoom(user, room)
            }
            "kick" -> {
                val userInfo = jsonMapper.treeToValue(packet.payload, UserActionEvent::class.java)

                if (user.role != "admin") {
                    println("!!! Access denied. ${user.name} (${session.remoteAddress}) try to kick ${userInfo.userId}")
                    return
                }

                kickUser(userInfo.userId)
            }
            "ban" -> {
                val userInfo = jsonMapper.treeToValue(packet.payload, UserActionEvent::class.java)

                if (user.role != "admin") {
                    println("!!! Access denied. ${user.name} (${session.remoteAddress}) try to ban ${userInfo.userId}")
                    return
                }

                banUser(userInfo.userId)
            }
            else -> {
                println("Unknown message type ${packet.type}")
            }
        }
    }

    override fun afterConnectionEstablished(session: WebSocketSession) {
        val user = createUser(session)
        userList.add(user)

        // Notify users
        notifyUserProfile(user)
        notifyUpdateUserRoomList(user)
        notifyUpdateUserList()
    }

    fun createUser(session: WebSocketSession): User {
        val p = session.principal

        var role = "user"
        val userName = if (p != null) {
            when (p) {
                is ChatPrincipal -> {
                    p.name
                }
                // It is X509 name
                else -> {
                    val extractCNRegex = "CN=([^,]*),?".toRegex()
                    val matches = extractCNRegex.find(p.name)

                    if (matches == null || matches.groups.size != 2) {
                        session.remoteAddress.toString()
                    } else {
                        val cn = matches.groupValues[1]

                        if (cn == "Admin") {
                            role = "admin"
                        }

                        cn
                    }
                }
            }
        } else {
            session.remoteAddress.toString()
        }

        return User(userName, role, session)
    }

    // Controllers
    fun sendMessageToRoom(message: Message, room: Room) {
        val jsonNode = jsonMapper.valueToTree<JsonNode>(message)

        broadcastPacketToRoom(room, Packet("message", jsonNode))
    }

    fun joinUserToRoom(user: User, room: Room) {
        if (isJoinedIn(user, room)) {
            // User is already joined to room
            return
        }

        room.users.add(user.name)

        println("User ${user.name} is joined to ${room.name}")
    }

    fun kickUser(userId: String) {
        val user = findUser(userId)

        if (user != null) {
            user.session.close()
        }
    }

    fun banUser(userId: String) {
        val user = findUser(userId)

        if (user != null) {
            user.session.close()

            val remoteAddress = user.session.remoteAddress;

            if (remoteAddress != null)
                banSystem.banUser(remoteAddress)
        }
    }

    fun notifyUpdateUserUserList(user: User) {
        val packet = getUserListPacket()

        sendPacketToUser(user, packet)
    }

    fun notifyUpdateUserList() {
        val packet = getUserListPacket()

        broadcastPacket(packet)
    }

    fun getUserListPacket(): Packet {
        val users = userList.map { UserView(it.name, it.role) }
                .toTypedArray()

        val userList = UserListPacket(users)

        val packet = Packet("userList", jsonMapper.valueToTree<JsonNode>(userList))
        return packet
    }

    fun notifyUserProfile(user: User) {
        val profile = UserProfile(user.name, user.role)

        val packet = Packet("yourProfile", jsonMapper.valueToTree<JsonNode>(profile))

        sendPacketToUser(user, packet)
    }

    fun notifyUpdateUserRoomList(user: User) {
    }

    fun notifyUpdateRoomList() {
    }

    // WS tools
    fun broadcastPacketToRoom(room: Room, packet: Packet) {
        val serializedPacket = jsonMapper.writeValueAsString(packet)
        val textMessage = TextMessage(serializedPacket)

        // TODO
        room.users.forEach { userId ->
            userList.filter { user -> user.name == userId }
                    .forEach { user ->
                user.session.sendMessage(textMessage)
            }
        }
    }

    fun broadcastPacket(packet: Packet) {
        val serializedPacket = jsonMapper.writeValueAsString(packet)
        val textMessage = TextMessage(serializedPacket)

        userList.forEach { user ->
            user.session.sendMessage(textMessage)
        }
    }

    fun sendPacketToUser(user: User, packet: Packet) {
        val serializedPacket = jsonMapper.writeValueAsString(packet)
        val textMessage = TextMessage(serializedPacket)
        user.session.sendMessage(textMessage)
    }

    // Tools
    fun findUser(session: WebSocketSession): User? {
        return userList.find { it.session == session }
    }

    fun findUser(userId: String): User? {
        return userList.find { it.name == userId }
    }

    fun findRoom(roomId: String): Room? {
        return roomList.find { it.id == roomId }
    }

    fun isJoinedIn(user: User, room: Room): Boolean {
        return roomList.any { it.users.contains(user.name) }
    }
}
