@echo off
set OPENSSL_CONF=C:\Bin\openssl-1.1\openssl.cnf

mkdir pki
cd pki
mkdir certs_by_serial
mkdir private
type nul > index.txt
echo 1000000000 >> serial

cd ..

REM Create CA
echo "Generate CA"
openssl ecparam -name secp521r1 -genkey -out ./pki/ca.key
openssl req -x509 -new -nodes -key ./pki/ca.key -days 3650 -out ./pki/ca.crt

REM  Create JAVA CA file
REM keytool -import -trustcacerts -file ca.crt -storetype PKCS12 -keystore ca.p12

REM Create Server
echo "Generate Server"
openssl ecparam -name secp521r1 -genkey -out ./pki/server.key
openssl req -new -nodes -key ./pki/server.key -out ./pki/server.csr
openssl ca -create_serial -in ./pki/server.csr -out ./pki/server.crt
openssl pkcs12 -export -inkey ./pki/server.key -in ./pki/server.crt -certfile ./pki/ca.crt -out ./pki/server.p12

REM Create Server 2
echo "Generate Server 2"
openssl ecparam -name prime256v1 -genkey -out ./pki/server2.key
openssl req -new -nodes -key ./pki/server2.key -out ./pki/server2.csr
openssl ca -in ./pki/server2.csr -out ./pki/server2.crt
openssl pkcs12 -export -inkey ./pki/server2.key -in ./pki/server2.crt -certfile ./pki/ca.crt -out ./pki/server2.p12

REM Create Client
echo "Generate Client 1"
openssl ecparam -name secp521r1 -genkey -out ./pki/client1.key
openssl req -new -nodes -key ./pki/client1.key -out ./pki/client1.csr
openssl ca -in ./pki/client1.csr -out ./pki/client1.crt
openssl pkcs12 -export -inkey ./pki/client1.key -in ./pki/client1.crt -certfile ./pki/ca.crt -out ./pki/client1.p12

REM Create server pkcs12
REM openssl pkcs12 -export -inkey server.key -in server.crt -certfile ca.crt -out server.p12


REM openssl ecparam -list_curves