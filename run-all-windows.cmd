@echo off

docker rm -f chat-server
docker rm -f chat-client

docker run -d --name chat-server --restart always -p 38443:38443 quckly/sne-chat-server:latest
docker run -d --name chat-client --restart always -p 8080:80 quckly/sne-chat-client:latest
