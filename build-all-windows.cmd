@echo off
echo Building docker images:

docker build -t quckly/sne-chat-client:latest -f .\Dockerfile.client .
docker build -t quckly/sne-chat-server:latest -f .\Dockerfile.server .
